/// Создаем и наполняем таблицу

CREATE TABLE foo(
    c1 INTEGER,
    c2 TEXT
    );

INSERT INTO foo
    SELECT
        i,
        md5(random()::text)
    FROM
        generate_series(1, 1000000) AS i;
        
/// Вывод плана полного селекта

test=# EXPLAIN SELECT * FROM foo;
                          QUERY PLAN
--------------------------------------------------------------
 Seq Scan on foo  (cost=0.00..18584.82 rows=1025082 width=36)


/// Добавляем немного значений, обновляем статистику 

INSERT INTO foo
    SELECT
        i,
        md5(random()::text)
    FROM
        generate_series(1, 10) AS i;
ANALYZE foo;

test=# EXPLAIN SELECT * FROM foo;
                          QUERY PLAN
--------------------------------------------------------------
 Seq Scan on foo  (cost=0.00..18334.10 rows=1000010 width=37)
(1 row)


/// Cравнение спрогнозированных значений с реальными

test=# EXPLAIN(ANALYZE) SELECT * FROM foo;
                                                   QUERY PLAN
----------------------------------------------------------------------------------------------------------------
 Seq Scan on foo  (cost=0.00..18334.10 rows=1000010 width=37) (actual time=0.007..215.096 rows=1000010 loops=1)
 Total runtime: 378.291 ms
(2 rows)


/// Запрос с WHERE без индексов

test=# EXPLAIN(ANALYZE) SELECT * FROM foo WHERE c1>500;
                                                  QUERY PLAN
--------------------------------------------------------------------------------------------------------------
 Seq Scan on foo  (cost=0.00..20834.12 rows=999484 width=37) (actual time=0.101..270.398 rows=999500 loops=1)
   Filter: (c1 > 500)
   Rows Removed by Filter: 510
 Total runtime: 435.795 ms
(4 rows)

/// Запрос с ORDER BY

test=# EXPLAIN(ANALYZE) SELECT * FROM foo ORDER BY c1;
                                                      QUERY PLAN
----------------------------------------------------------------------------------------------------------------------
 Sort  (cost=145338.51..147838.54 rows=1000010 width=37) (actual time=972.701..1248.911 rows=1000010 loops=1)
   Sort Key: c1
   Sort Method: external merge  Disk: 45952kB
   ->  Seq Scan on foo  (cost=0.00..18334.10 rows=1000010 width=37) (actual time=0.010..230.673 rows=1000010 loops=1)
 Total runtime: 1422.279 ms
(5 rows)


/// Запросы с JOIN
/// Создадим дополнительную таблицу


CREATE TABLE bar(
    c1 INTEGER,
    c2 BOOLEAN
    );

INSERT INTO bar
    SELECT
        i,
        i%2 = 1
    FROM
        generate_series(1, 500000) AS i;
        
/// Посмотрим план запроса на разныз JOIN

