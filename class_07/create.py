from peewee import *
from datetime import date

db = PostgresqlDatabase(database='shop_datas', user='postgres',password='postgres')

# Создаем таблицы

class Customers(Model):
    cust_id = AutoField()
    first_nm = CharField()
    last_nm = CharField()
    class Meta:
        database = db
    
class Orders(Model):
    order_id = AutoField()
    cust_id = ForeignKeyField(Customers, to_field='cust_id')
    order_dttm = DateTimeField()
    status = CharField()
    class Meta:
        database = db
        
class Goods(Model):
    good_id = AutoField()
    vendor = CharField()
    name = CharField()
    description = CharField()
    class Meta:
        database = db
        
class Order_items(Model):
    order_item_id = AutoField()
    order_id = ForeignKeyField(Orders, to_field='order_id')
    good_id = ForeignKeyField(Goods, to_field='good_id')
    quantity = IntegerField()
    class Meta:
        database = db
    
        
Customers.create_table()
Orders.create_table()
Goods.create_table()
Order_items.create_table()


# Наполняем их данными

Customers.create(first_nm='Joey', last_nm='Tribbiani')
Customers.create(first_nm='Chandler', last_nm='Bing')
Customers.create(first_nm='Phoebe', last_nm='Buffay')

Orders.create(cust_id=2, order_dttm=date(2017,6,4), status='Done')
Orders.create(cust_id=3, order_dttm=date(2017,11,4), status='Awaiting')
Orders.create(cust_id=1, order_dttm=date(2017,6,9), status='Done')
Orders.create(cust_id=2, order_dttm=date(2017,10,9), status='Done')

Goods.create(vendor='Apple', name='MacBook', description='laptop')
Goods.create(vendor='Lenovo', name='ThinkPad', description='another laptop')

Order_items.create(order_id=1, good_id=2,quantity=2)
Order_items.create(order_id=2, good_id=1,quantity=1)
Order_items.create(order_id=3, good_id=2,quantity=1)
Order_items.create(order_id=4, good_id=2,quantity=1)