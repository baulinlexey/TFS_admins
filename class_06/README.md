1. Код создания таблиц лежит в ```create.py```

2. Скрипт получения csv файла лежит в ```to_csv.py```, сам csv в ```result.csv```.

3. Заполненные таблицы выглядят так:
```
shop_data=# SELECT * FROM customers;
SELECT * FROM orders;SELECT * FROM goods; SELECT * FROM order_items;

 cust_id | first_nm | last_nm
---------+----------+---------
       1 | Alex     | Sergeev
       4 | John     | Smith
       2 | William  | Kassi
(3 rows)

 order_id | cust_id |     order_dttm      |  status
----------+---------+---------------------+----------
        1 |       1 | 2017-09-28 19:00:00 | Done
        2 |       1 | 2017-09-30 19:00:00 | Awaiting
        3 |       2 | 2017-10-30 19:00:00 | Done
(3 rows)

 good_id |  vendor   |    name    |      description
---------+-----------+------------+------------------------
       1 | Apple     | MacBook 17 | laptop
       2 | Microsoft | Surface    | another laptop
       3 | Dell      | Vostro     | another another laptop
(3 rows)

 order_item_id | order_id | good_id | quantity
---------------+----------+---------+----------
             1 |        1 |       3 |        3
             2 |        2 |       2 |        1
             3 |        3 |       1 |        1
(3 rows)

```