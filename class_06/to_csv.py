import psycopg2
import csv

try:
    conn = psycopg2.connect(host="127.0.0.1",database="shop_data",
                            user="postgres",password="postgres")
except:
    print( "I am unable to connect to the database")

try:
    cur = conn.cursor()  

except:
    print('error in cur')

    
commands = (
        """
    SELECT 
        O.order_id,
        C.first_nm,
        g.name,
        g.vendor
    FROM customers as C
        JOIN orders as O
        ON C.cust_id = O.cust_id
        LEFT JOIN order_items as OI
        ON OI.order_id = O.order_id
        LEFT JOIN goods AS g
        ON OI.good_id = g.good_id;
        """)
    
try:
    cur.execute(commands)
    rows = cur.fetchall()
    print(type(rows), rows, sep='\n')
    cur.close()
    conn.commit()
except (Exception, psycopg2.DatabaseError) as error:
    print(error)
    
with open('result.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile, delimiter=',',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
    for row in rows:
        writer.writerow(row)

print('success!')

