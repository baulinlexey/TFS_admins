import psycopg2

try:
    conn = psycopg2.connect(host="127.0.0.1",database="shop_data",
                            user="postgres",password="postgres")
except:
    print( "I am unable to connect to the database")

try:
    cur = conn.cursor()  

except:
    print('error in cur')

    
commands = (
        """
        CREATE TABLE customers (
            cust_id    SERIAL       PRIMARY KEY,
            first_nm   VARCHAR(100) NOT NULL,
            last_nm    VARCHAR(100) NOT NULL
            )
        """,

        """ 
        CREATE TABLE orders (
            order_id    SERIAL      PRIMARY KEY,
            cust_id     SERIAL      NOT NULL,
            FOREIGN KEY (cust_id) 
            REFERENCES customers (cust_id)
            ON UPDATE CASCADE ON DELETE CASCADE,
            order_dttm  TIMESTAMP   NOT NULL,
            status      VARCHAR(20) NOT NULL  
            )
        """,

        """
        CREATE TABLE goods (
            good_id     SERIAL       PRIMARY KEY,
            vendor      VARCHAR(100) NOT NULL,
            name        VARCHAR(100) NOT NULL,
            description VARCHAR(300) NOT NULL
            )
        """,

        """
        CREATE TABLE order_items (
            order_item_id SERIAL   PRIMARY KEY,
            order_id      SERIAL   NOT NULL,
            FOREIGN KEY (order_id)
            REFERENCES orders (order_id)
            ON UPDATE CASCADE ON DELETE CASCADE,
            good_id       SERIAL  NOT NULL,
            FOREIGN KEY (good_id) 
            REFERENCES goods (good_id)
            ON UPDATE CASCADE ON DELETE CASCADE,
            quantity       INTEGER NOT NULL
            )
        """)
    
try:
    for command in commands:
        cur.execute(command)
    cur.close()
    conn.commit()
except (Exception, psycopg2.DatabaseError) as error:
    print(error)

print('success!')

