#!/bin/bash

ret=$(ps -a | grep [h]ttpd| wc -l)
if [ "$ret" -eq 0 ]

then {
	echo $(date +"%y-%m-%d %T") "httpd was not running, starting..." >>/root/httpd_start.log
	sudo /root/homework/materials/class03/src/tinyhttpd/tinyhttpd/httpd
    sleep 1  
	exit 1
}
fi;