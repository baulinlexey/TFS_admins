CREATE DATABASE test;
\c test; 
CREATE TABLE t (
    a INTEGER, 
    b TEXT, 
    c BOOLEAN
    );

INSERT INTO t(a,b,c)
    SELECT 
        s.id, 
        chr((32+random()*94)::integer), 
        random() < 0.01
    FROM generate_series(1,100000) AS s(id)
    ORDER BY random();

CREATE INDEX ON t(a);
        
ANALYZE t;


/// Условие только на а -> индексное сканирование


test=# explain (costs off) select * from t where a =1;
          QUERY PLAN
-------------------------------
 Index Scan using t_a_idx on t
   Index Cond: (a = 1)
(2 rows)


/// Условие только на а, много значений -> сканирование по битовой карте


test=# explain (costs off) select * from t where a < 1000;
             QUERY PLAN
------------------------------------
 Bitmap Heap Scan on t
   Recheck Cond: (a < 1000)
   ->  Bitmap Index Scan on t_a_idx
         Index Cond: (a < 1000)
(4 rows)


/// Если строк, удовлетворяющих условию слишком много, используется просто последовательное сканирование 


test=# explain (costs off) select * from t where a <= 40000;
       QUERY PLAN
------------------------
 Seq Scan on t
   Filter: (a <= 40000)
(2 rows)




/// Проиндексируем еще одну переменную

CREATE index ON t(b);
analyze t;


/// Объединяются две версии битовых карт


test=# explain (costs off) select * from t where a <= 100 and b = 'a';
                    QUERY PLAN
--------------------------------------------------
 Bitmap Heap Scan on t
   Recheck Cond: ((a <= 100) AND (b = 'a'::text))
   ->  BitmapAnd
         ->  Bitmap Index Scan on t_a_idx
               Index Cond: (a <= 100)
         ->  Bitmap Index Scan on t_b_idx
               Index Cond: (b = 'a'::text)
(7 rows)


/// Если запрос маленький используется индексное сканирование


test=# explain (costs off) select * from t where a <= 1 and b = 'a';
          QUERY PLAN
-------------------------------
 Index Scan using t_a_idx on t
   Index Cond: (a <= 1)
   Filter: (b = 'a'::text)
(3 rows)


/// Можно посмотреть, насколько упорядочены данные


test=# select attname, correlation from pg_stats where tablename = 't';
 attname | correlation
---------+-------------
 a       |  0.00157745
 b       |  0.00726183
 c       |    0.980651  / Тут близко к еденице -> максимально упорядочены
(3 rows)
