### 3адание 1 и начало 4


```BASH
uwsgi --plugins=python --http-socket=0.0.0.0:80 --wsgi-file /opt/webcode/former/process/webrunner.py 
      --static-map /form=/opt/webcode/former/form/index.html --processes=5 --master --pidfile=/tmp/formdig.pid --vacuum --max-requests=5000
```

|uwsgi||
|-----------------------------------------------------|------------------------------------------------------------------------------
|--plugins=python                                     |Загружает UWSGI-плагин Python|
|--http-socket=0.0.0.0:80                             |Привязывает uWSGI к определенному HTTP-сокету|
|--wsgi-file opt/webcode/former/process/webrunner.py	 | Запуск приложения|
|--static-map form=/opt/webcode/former/form/index.html|Точка монтирования|
|--processes=5                                        |Количество процессов, созданных для того, чтобы принимать запросы|
|--master                                             |Активирует/дезактивирует главный процесс uWSGI|
|--pidfile=/tmp/formdig.pid	                         |Mесто для записи PID файла|
|--vacuum	                                         |Удаляет все сгенерированные pidfiles/sockets при выходе|
|--max-requests=5000`                                 |Автоматически перезапускает процессы после обработки заданного количества запросов|


### Задание 2

http://s-21.fintech-admin.m1.tinkoff.cloud/process

### Задание 3


```HTML
Method: GET
Get content: /process?Name=Alex&Age=22
Post content: 
```

